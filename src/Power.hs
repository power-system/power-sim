{-# OPTIONS_HADDOCK hide, prune, ignore-exports #-}
-- |
-- Module      : Power
-- Description : Short description
-- Copyright   : (c) SREC, 2021
-- License     : BSD-3-Clause
-- Maintainer  : shashank@srec.in
-- Stability   : experimental
-- Portability : POSIX
-- Here is a longer description of this module, containing some
-- commentary with @some markup@.
-- = Heading level 1 with some __bold__
-- Something underneath the heading.
--
-- == /Subheading/
-- More content.
--
-- === Subsubheading
-- >>> examples are only allowed at the start of paragraphs

module Power where


-- |
-- = Heading level 1 with some /emphasis/
-- Something underneath the heading.
--
-- == /Subheading/
-- More content.
--
-- === Subsubheading
-- Even more content.
readx :: Int -> Int
readx x = x

{-|
Sample

-}
