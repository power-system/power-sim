{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
module Power.Model.Network where

import Data.Graph.Inductive.Graph
import Data.Graph.Inductive.PatriciaTree
import Power.State
import Data.Complex
import qualified Data.Text as T
import Data.UUID
import Data.List
import Data.Maybe
import Optics
import Optics.TH
import Data.Graph.Inductive.NodeMap
import Control.Monad


import Power.Model.EnergySource
import Power.Model.EnergyConsumer
import Power.Model.SynchronousMachine
import Power.Model.ASynchronousMachine
import Power.Model.BusbarSection
import Power.Model.PowerTransformer
import Power.Model.ACLineSegment

data NetworkType = YBUS | ZBUS | ZBUS3
type PSGraph = Gr UUID (Complex Double)
data Network = Network 
  { graphs :: PSGraph

  } deriving (Show)


createEnergyConsumerEdges :: EnergyConsumer -> NodeMap UUID -> LNode UUID -> Maybe (LEdge (Complex Double))
createEnergyConsumerEdges es nmap (i,n) = mkEdge nmap (n, nil, 1 :+ 8)

createBusbarSectionEdges :: BusbarSection -> NodeMap UUID -> LNode UUID -> Maybe (LEdge (Complex Double))
createBusbarSectionEdges es nmap (i,n) = mkEdge nmap (n, nil, 2 :+ 4)

createSynchronousMachineEdges :: SynchronousMachine -> NodeMap UUID -> LNode UUID -> Maybe (LEdge (Complex Double))
createSynchronousMachineEdges es nmap (i,n) = mkEdge nmap (n, nil, synchronousMachineZpu es 100 11.0)


createASynchronousMachineEdges :: ASynchronousMachine -> NodeMap UUID -> LNode UUID -> Maybe (LEdge (Complex Double))
createASynchronousMachineEdges es nmap (i,n) = mkEdge nmap (n, nil, aSynchronousMachineZpu es 100 11.0)


createTransformerWindingLinks :: PowerTransformer -> Network -> [LEdge (Complex Double)]
createTransformerWindingLinks tx net = 
  let 
    nmap = fromGraph (graphs net) 
    nlist = labNodes $ graphs net
    windings = _powerTransformerWindings tx
    id = _powerTransformerID tx
  in 
    fromMaybe [] $ mkEdges nmap $ map (\wind -> let 
        wid = _transformerWindingID wind
        zpu = transformerEndZpu wind 100.0 10.0
        in (id, wid, zpu)) windings


createEnergySourceEdges :: EnergySource -> NodeMap UUID -> LNode UUID -> Maybe (LEdge (Complex Double))
createEnergySourceEdges es nmap (i,n) = let 
  r = energySourceRpu es 100 11.0 
  x = energySourceXpu es 100 11.0 
  in 
  mkEdge nmap (n, nil, r :+ x)

createNetwork :: State -> NetworkType ->  Network
createNetwork st _ = let 
  groundNodeID = nil
  networkEnergySources = _energySources st
  nMap = new :: NodeMap UUID
  (gnode, nmap0) = mkNode nMap nil 
  (esNodes, nmap1) = mkNodes nmap0 $ map _energySourceID $ _energySources st
  (loadNodes, nmap2) = mkNodes nmap1 $ map _energyConsumerID $ _energyConsumers st
  (synchNodes, nmap3) = mkNodes nmap2 $ map _synchronousMachineID $ _synchronousMachines st
  (asynchNodes, nmap4) = mkNodes nmap3 $ map _aSynchronousMachineID $ _aSynchronousMachines st
  (busNodes, nmap5) = mkNodes nmap4 $ map _busbarSectionID $ _busbarSections st
  (txNodes, nmap6) = mkNodes nmap5 $ map _powerTransformerID $ _powerTransformers st
  (windNodes, nmap7) = mkNodes nmap6 $ map _transformerWindingID $ concat $ map _powerTransformerWindings $ _powerTransformers st
  in 
  Network $ mkGraph ([gnode] ++ esNodes ++ loadNodes ++ busNodes ++ synchNodes ++ asynchNodes ++ txNodes ++ windNodes) []
  --Network $ mkGraph ([gnode] ++ busNodes ++ esNodes ++ asynchNodes ++ synchNodes) []


getEnergySourceFromState :: State -> UUID -> Maybe EnergySource
getEnergySourceFromState st id = es
  where
  esList = map _energySourceID $ _energySources st
  es = find (\x -> if _energySourceID x == id then True else False) $ _energySources st

getEnergyConsumerFromState :: State -> UUID -> Maybe EnergyConsumer
getEnergyConsumerFromState st id = es
  where
  esList = map _energyConsumerID $ _energyConsumers st
  es = find (\x -> if _energyConsumerID x == id then True else False) $ _energyConsumers st

getBusbarSectionFromState :: State -> UUID -> Maybe BusbarSection
getBusbarSectionFromState st id = es
  where
  esList = map _busbarSectionID $ _busbarSections st
  es = find (\x -> if _busbarSectionID x == id then True else False) $ _busbarSections st


getSynchronousMachineFromState :: State -> UUID -> Maybe SynchronousMachine
getSynchronousMachineFromState st id = es
  where
  esList = map _synchronousMachineID $ _synchronousMachines st
  es = find (\x -> if _synchronousMachineID x == id then True else False) $ _synchronousMachines st

getASynchronousMachineFromState :: State -> UUID -> Maybe ASynchronousMachine
getASynchronousMachineFromState st id = es
  where
  esList = map _aSynchronousMachineID $ _aSynchronousMachines st
  es = find (\x -> if _aSynchronousMachineID x == id then True else False) $ _aSynchronousMachines st


getLineConnectionFromUUID :: UUID -> Maybe Int -> State -> UUID
getLineConnectionFromUUID id s st = let 
    txs = _powerTransformers st
    istx = isPowerTransformer id txs
    in 
    if istx then 
            getPowerTransformerWindingID id (fromMaybe 0 s) txs
    else id

getLineEdges :: State -> Network -> [LEdge ℂ ]
getLineEdges st net = let
  nmap = fromGraph (graphs net) 
  in
  catMaybes $ map (\x -> let 
    fromSeq = _acLineSegmentFromSequence x
    toSeq = _acLineSegmentFromSequence x
    fromUUID = getLineConnectionFromUUID (_acLineSegmentFrom x) fromSeq st
    toUUID =   getLineConnectionFromUUID (_acLineSegmentTo x) toSeq st
    fromTx = isPowerTransformer fromUUID $ _powerTransformers st
    toTx = isPowerTransformer toUUID $ _powerTransformers st
    in mkEdge nmap (fromUUID, toUUID, 0.001 :+ 0.005)) $ _lines st

createNetworkEdges :: State -> Network -> Network
createNetworkEdges st net = let 
  nmap = fromGraph (graphs net) 
  esList = map _energySourceID $ _energySources st
  nlist = labNodes $ graphs net
  
  eds = filter (\(n,l) -> case getEnergySourceFromState st l of 
    Nothing -> False
    Just es -> True) nlist
  esEds = catMaybes $ map (\ln@(n,l) -> case getEnergySourceFromState st l of 
    Nothing -> Nothing
    Just es -> createEnergySourceEdges es nmap ln ) eds
  lds = filter (\(n,l) -> case getEnergyConsumerFromState st l of 
    Nothing -> False
    Just es -> True) nlist
  lsEds = catMaybes $ map (\ln@(n,l) -> case getEnergyConsumerFromState st l of 
    Nothing -> Nothing
    Just es -> createEnergyConsumerEdges es nmap ln ) lds
    
  bbs = filter (\(n,l) -> case getBusbarSectionFromState st l of 
    Nothing -> False
    Just es -> True) nlist
  bsEds = catMaybes $ map (\ln@(n,l) -> case getBusbarSectionFromState st l of 
    Nothing -> Nothing
    Just es -> createBusbarSectionEdges es nmap ln ) bbs
  syncs = filter (\(n,l) -> case getSynchronousMachineFromState st l of 
    Nothing -> False
    Just es -> True) nlist
  synchEds = catMaybes $ map (\ln@(n,l) -> case getSynchronousMachineFromState st l of 
    Nothing -> Nothing
    Just es -> createSynchronousMachineEdges es nmap ln ) syncs
  asyncs = filter (\(n,l) -> case getASynchronousMachineFromState st l of 
    Nothing -> False
    Just es -> True) nlist
  asynchEds = catMaybes $ map (\ln@(n,l) -> case getASynchronousMachineFromState st l of 
    Nothing -> Nothing
    Just es -> createASynchronousMachineEdges es nmap ln ) asyncs
  windLinks = concat $ map (\x -> createTransformerWindingLinks x net) $ _powerTransformers st
  
  lineEdges = getLineEdges st net
  allEdges = esEds ++ lsEds ++ bsEds ++ lineEdges ++ synchEds ++asynchEds ++ windLinks
  -- allEdges = bsEds ++ lineEdges ++ esEds ++ asynchEds ++ synchEds
  in Network $ insEdges allEdges $ graphs net
