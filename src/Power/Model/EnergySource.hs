{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
module Power.Model.EnergySource where


-- import Control.Lens
import Control.Applicative
import Data.Foldable
import Data.Monoid
import qualified Data.Text as T
import Optics
import Optics.TH
import qualified Data.ByteString.Char8 as BS
import System.Directory
import Data.Yaml
import Data.Either
import Data.UUID
import Data.Complex
import Power.Model.DiagramObject

type ℝ = Double
type ℂ = Complex Double

data EnergySource = EnergySource 
  { _energySourceID :: UUID
  , _energySourceName :: T.Text
  , _energySourceTag :: T.Text
  , _energySourceDescription :: Maybe T.Text
  , _energySourceVoltage :: ℝ
  , _energySourceBaseVoltage :: ℝ
  , _energySource3PhMVA :: ℝ
  , _energySource1PhMVA :: ℝ
  , _energySourceXbyR :: ℝ
  , _energySourceX0byR0 :: ℝ
  , _energySourceDiagramObject :: DiagramObject
  , _energySourceDiagramTagObject :: DiagramObject
  } deriving (Show, Eq)


instance ToJSON EnergySource where
  toJSON EnergySource {..} = object 
    [ "id" .= _energySourceID
    , "name" .= _energySourceName
    , "tag"  .= _energySourceTag
    , "description" .= _energySourceDescription
    , "voltage"     .= _energySourceVoltage
    , "base-voltage"     .= _energySourceVoltage
    , "mva-3phase"  .= _energySource3PhMVA
    , "mva-1phase"  .= _energySource3PhMVA
    , "xbyr"         .= _energySourceXbyR
    , "x0byr0"         .= _energySourceX0byR0
    , "symbol-object"        .= _energySourceDiagramObject
    , "rating-object"        .= _energySourceDiagramTagObject
    ]

instance FromJSON EnergySource where
  parseJSON (Object o) = EnergySource
    <$> o .: "id"
    <*> o .: "name"
    <*> o .: "tag"
    <*> o .: "description"
    <*> o .: "voltage"
    <*> o .: "base-voltage"
    <*> o .: "mva-3phase"
    <*> o .: "mva-1phase"
    <*> o .: "xbyr"
    <*> o .: "x0byr0"
    <*> o .: "symbol-object"
    <*> o .: "rating-object"

makeLenses ''EnergySource




loadEnergySource :: String -> IO (Either ParseException EnergySource)
loadEnergySource fname = do 
  content <- BS.readFile fname
  es <- Data.Yaml.decodeFileEither fname
  return es



writeEnergySource :: EnergySource -> IO ()
writeEnergySource = encodeFile "sample.yaml" 


filterEnergySource :: Either ParseException EnergySource -> Bool
filterEnergySource x 
  | isLeft x = False
  | isRight x = True


loadAllEnergySources :: FilePath -> IO [EnergySource]
loadAllEnergySources fpath = do
  setCurrentDirectory fpath
  contents <- listDirectory fpath
  yamlContent <- mapM loadEnergySource contents
  let filtered = rights yamlContent
  return filtered

writeAllEnergySources :: FilePath -> [EnergySource] -> IO ()
writeAllEnergySources fpath es = do
  forM_ es writeEnergySource



energySourceR :: EnergySource -> ℝ
energySourceR es = r 
  where
    v = _energySourceVoltage es
    alp = _energySourceXbyR es
    mva = _energySource3PhMVA es
    r = v*v / (mva * (sqrt (1 + alp * alp))) 


energySourceX :: EnergySource -> ℝ
energySourceX es = x 
  where 
    r = energySourceR es
    alp = _energySourceXbyR es
    x = r * alp

energySourceR0 :: EnergySource -> ℝ
energySourceR0 es = r 
  where
    v = _energySourceVoltage es
    alp = _energySourceX0byR0 es
    mva = _energySource1PhMVA es
    r = v*v / (mva * (sqrt (1 + alp * alp))) 


energySourceX0 :: EnergySource -> ℝ
energySourceX0 es = x 
  where 
    r = energySourceR0 es
    alp = _energySourceX0byR0 es
    x = r * alp

energySourceRpu :: EnergySource -> ℝ -> ℝ -> ℝ
energySourceRpu es mva v= rpu
  where
    r = energySourceR es
    zb = v*v/mva
    rpu = r / zb

energySourceXpu :: EnergySource -> ℝ -> ℝ -> ℝ
energySourceXpu es mva v= xpu
  where
    x = energySourceX es
    zb = v*v/mva
    xpu = x / zb

energySourceABCD :: EnergySource -> ℝ -> ℝ -> (ℂ, ℂ, ℂ, ℂ)
energySourceABCD es mva v = abcd
  where
    rpu = energySourceRpu es mva v
    xpu = energySourceRpu es mva v
    a = 1 :+ 0
    b = rpu :+ xpu
    c = 0 
    d = 0
    abcd = (a, b, c, d)

