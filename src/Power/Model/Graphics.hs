{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
module Power.Model.Graphics where


import Power.State
import Power.Model.EnergySource
import Power.Model.EnergyConsumer
import Power.Model.SynchronousMachine
import Power.Model.ASynchronousMachine
import Power.Model.BusbarSection
import Power.Model.PowerTransformer
import Power.Model.ACLineSegment
import Power.Model.DiagramObject
import Diagrams.Backend.SVG
import Diagrams.Prelude
import Diagrams.Backend.SVG.CmdLine


--drawNetwork :: State -> IO () 

drawEnergySource :: Int -> Diagram B
drawEnergySource x = rect 10 20  # fc green

drawEnergySourcePoints :: [EnergySource] -> [Point V2 Double]
drawEnergySourcePoints sources = map (\es -> let 
    obj = _energySourceDiagramObject es
    xpos = fromIntegral $ _diagramObjectXPos obj
    ypos = fromIntegral $ _diagramObjectYPos obj
    ss = [drawEnergySource x| x<-[1..(length sources)]]
    in 
    mkP2 xpos ypos)sources


drawEnergySources :: [EnergySource] -> Diagram B
drawEnergySources sources = let
  points = drawEnergySourcePoints sources
  ss = map drawEnergySource [1..(length sources)]
  in atPoints points ss

