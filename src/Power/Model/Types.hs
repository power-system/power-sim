module Power.Model.Types where


import Diagrams.Prelude
import Diagrams.Backend.SVG




class Drawable d where
  draw :: d -> Diagram B


class Movable d where
  move :: d -> d
  moveBy :: d -> d
