{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
module Power.Model.EnergyConsumer where


-- import Control.Lens
import Control.Applicative
import Data.Foldable
import Data.Monoid
import qualified Data.Text as T
import Optics
import Optics.TH
import qualified Data.ByteString.Char8 as BS
import System.Directory
import Data.Yaml
import Data.Either
import Data.UUID
import Power.Model.DiagramObject
type ℝ = Double

data EnergyConsumer = EnergyConsumer 
  { _energyConsumerID :: UUID
  , _energyConsumerName :: T.Text
  , _energyConsumerTag :: T.Text
  , _energyConsumerDescription :: Maybe T.Text
  , _energyConsumerVoltage :: ℝ
  , _energyConsumer3PhMVA :: ℝ
  , _energyConsumerPf :: ℝ
  , _energyConsumerDiagramObject :: DiagramObject
  , _energyConsumerDiagramTagObject :: DiagramObject
  } deriving (Show, Eq)


instance ToJSON EnergyConsumer where
  toJSON EnergyConsumer {..} = object 
    [ "id" .= _energyConsumerID
    , "name" .= _energyConsumerName
    , "tag"  .= _energyConsumerTag
    , "description" .= _energyConsumerDescription
    , "voltage"     .= _energyConsumerVoltage
    , "mva-3phase"  .= _energyConsumer3PhMVA
    , "power-factor"  .= _energyConsumerPf
    , "symbol-object"        .= _energyConsumerDiagramObject
    , "rating-object"        .= _energyConsumerDiagramTagObject
    ]

instance FromJSON EnergyConsumer where
  parseJSON = withObject "EnergyConsumer" $ \o -> EnergyConsumer
    <$> o .: "id"
    <*> o .: "name"
    <*> o .: "tag"
    <*> o .: "description"
    <*> o .: "voltage"
    <*> o .: "mva-3phase"
    <*> o .: "power-factor"
    <*> o .: "symbol-object"
    <*> o .: "rating-object"

makeLenses ''EnergyConsumer




loadEnergyConsumer :: String -> IO (Either ParseException EnergyConsumer)
loadEnergyConsumer fname = do 
  content <- BS.readFile fname
  let es = Data.Yaml.decodeFileEither fname
  es



writeEnergyConsumer :: EnergyConsumer -> IO ()
writeEnergyConsumer = encodeFile "sample.yaml" 


filterEnergyConsumer :: Either ParseException EnergyConsumer -> Bool
filterEnergyConsumer x 
  | isLeft x = False
  | isRight x = True


loadAllEnergyConsumers :: FilePath -> IO [EnergyConsumer]
loadAllEnergyConsumers fpath = do
  setCurrentDirectory fpath
  contents <- listDirectory fpath
  yamlContent <- mapM loadEnergyConsumer contents
  let filtered = rights yamlContent
  return filtered

writeAllEnergyConsumers :: FilePath -> [EnergyConsumer] -> IO ()
writeAllEnergyConsumers fpath es = do
  forM_ es writeEnergyConsumer
