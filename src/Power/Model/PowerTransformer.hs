{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
module Power.Model.PowerTransformer where


-- import Control.Lens
import Control.Applicative
import Data.Foldable
import Data.Monoid
import qualified Data.Text as T
import Optics
import Optics.TH
import qualified Data.ByteString.Char8 as BS
import System.Directory
import Data.Yaml
import Data.Either
import Data.UUID
import Power.Model.DiagramObject
import Data.Complex
type ℝ = Double

data TransformerWinding = TransformerWinding
  { _transformerWindingID :: UUID
  , _transformerWindingSequenceNumber :: Int
  , _transformerWindingVoltage :: ℝ
  , _transformerWindingRating :: ℝ
  , _transformerWindingR :: ℝ
  , _transformerWindingR0 :: ℝ
  , _transformerWindingX :: ℝ
  , _transformerWindingX0 :: ℝ
  , _transformerWindingG :: ℝ
  , _transformerWindingG0 :: ℝ
  , _transformerWindingB :: ℝ
  , _transformerWindingB0 :: ℝ
  } deriving (Show, Eq)

data PowerTransformer = PowerTransformer 
  { _powerTransformerID :: UUID
  , _powerTransformerName :: T.Text
  , _powerTransformerTag :: T.Text
  , _powerTransformerDescription :: Maybe T.Text
  , _powerTransformerWindings :: [TransformerWinding]
  , _powerTransformerDiagramObject :: DiagramObject
  , _powerTransformerDiagramTagObject :: DiagramObject
  } deriving (Show, Eq)


instance ToJSON TransformerWinding where
  toJSON TransformerWinding {..} = object 
    [ "id" .= _transformerWindingID
    , "sequence-number" .= _transformerWindingSequenceNumber
    , "voltage" .= _transformerWindingVoltage
    , "rating" .= _transformerWindingRating
    , "r" .= _transformerWindingR
    , "r0" .= _transformerWindingR0
    , "x" .= _transformerWindingX
    , "x0" .= _transformerWindingX0
    , "g" .= _transformerWindingG
    , "g0" .= _transformerWindingG0
    , "b" .= _transformerWindingB
    , "b0" .= _transformerWindingB0
    ]

instance FromJSON TransformerWinding where
  parseJSON = withObject "TransformerWinding" $ \o -> TransformerWinding
    <$> o .: "id"
    <*> o .: "sequence-number"
    <*> o .: "voltage"
    <*> o .: "rating"
    <*> o .: "r"
    <*> o .: "r0"
    <*> o .: "x"
    <*> o .: "x0"
    <*> o .: "g"
    <*> o .: "g0"
    <*> o .: "b"
    <*> o .: "b0"


instance ToJSON PowerTransformer where
  toJSON PowerTransformer {..} = object 
    [ "id" .= _powerTransformerID
    , "name" .= _powerTransformerName
    , "tag"  .= _powerTransformerTag
    , "description" .= _powerTransformerDescription
    , "windings" .= _powerTransformerWindings
    , "symbol-object"        .= _powerTransformerDiagramObject
    , "rating-object"        .= _powerTransformerDiagramTagObject
    ]

instance FromJSON PowerTransformer where
  parseJSON = withObject "PowerTransformer" $ \o -> PowerTransformer
    <$> o .: "id"
    <*> o .: "name"
    <*> o .: "tag"
    <*> o .: "description"
    <*> o .: "windings"
    <*> o .: "symbol-object"
    <*> o .: "rating-object"

makeLenses ''PowerTransformer
makeLenses ''TransformerWinding




isPowerTransformer :: UUID -> [PowerTransformer] -> Bool
isPowerTransformer id txs = elem id $ map _powerTransformerID txs


getPowerTransformer :: UUID -> [PowerTransformer] -> Maybe PowerTransformer
getPowerTransformer id txs = find (\x -> _powerTransformerID x == id) txs



getWindingUUID :: Maybe PowerTransformer -> Int -> Maybe UUID
getWindingUUID tx s = case tx of
  Just w -> let 
     windings = _powerTransformerWindings w
     in 
     if (s <= length windings) then 
             Just (_transformerWindingID (windings !! s))
     else
             Nothing
  Nothing -> Nothing
  
getPowerTransformerWindingID :: UUID -> Int -> [PowerTransformer] -> UUID
getPowerTransformerWindingID id s txs = let
   tx = getPowerTransformer id txs
   wid = getWindingUUID tx s
   in case wid of 
     Just w -> w
     Nothing -> nil


loadPowerTransformer :: String -> IO (Either ParseException PowerTransformer)
loadPowerTransformer fname = do 
  content <- BS.readFile fname
  let es = Data.Yaml.decodeFileEither fname
  es



writePowerTransformer :: PowerTransformer -> IO ()
writePowerTransformer = encodeFile "sample.yaml" 


filterPowerTransformer :: Either ParseException PowerTransformer -> Bool
filterPowerTransformer x 
  | isLeft x = False
  | isRight x = True


loadAllPowerTransformers :: FilePath -> IO [PowerTransformer]
loadAllPowerTransformers fpath = do
  setCurrentDirectory fpath
  contents <- listDirectory fpath
  putStrLn $ show contents
  yamlContent <- mapM loadPowerTransformer contents
  let filtered = rights yamlContent
  return filtered

writeAllPowerTransformers :: FilePath -> [PowerTransformer] -> IO ()
writeAllPowerTransformers fpath es = do
  forM_ es writePowerTransformer


transformerEndZpu :: TransformerWinding -> ℝ -> ℝ -> Complex Double
transformerEndZpu wind mva v = zpu where
  zb = v*v/mva
  r = _transformerWindingR wind
  x = _transformerWindingX wind
  zpu = r :+ x
