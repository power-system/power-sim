{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
module Power.Model.ASynchronousMachine where


-- import Control.Lens
import Control.Applicative
import Data.Foldable
import Data.Monoid
import qualified Data.Text as T
import Optics
import Optics.TH
import qualified Data.ByteString.Char8 as BS
import System.Directory
import Data.Yaml
import Data.Either
import Data.UUID
import Power.Model.DiagramObject

import Data.Complex
type ℝ = Double

data ASynchronousMachine = ASynchronousMachine 
  { _aSynchronousMachineID :: UUID
  , _aSynchronousMachineName :: T.Text
  , _aSynchronousMachineTag :: T.Text
  , _aSynchronousMachineDescription :: Maybe T.Text
  , _aSynchronousMachineBaseVoltage :: ℝ
  , _aSynchronousMachineVoltage :: ℝ
  , _aSynchronousMachineRating :: ℝ
  , _aSynchronousMachinePF :: ℝ
  , _aSynchronousMachineEfficiency :: ℝ
  , _aSynchronousMachineLRR :: ℝ
  , _aSynchronousMachinePoles :: Int
  , _aSynchronousMachineDiagramObject :: DiagramObject
  , _aSynchronousMachineDiagramTagObject :: DiagramObject
  } deriving (Show, Eq)


instance ToJSON ASynchronousMachine where
  toJSON ASynchronousMachine {..} = object 
    [ "id" .= _aSynchronousMachineID
    , "name" .= _aSynchronousMachineName
    , "tag"  .= _aSynchronousMachineTag
    , "description" .= _aSynchronousMachineDescription
    , "base-voltage"     .= _aSynchronousMachineBaseVoltage
    , "voltage"     .= _aSynchronousMachineVoltage
    , "rating"  .= _aSynchronousMachineRating
    , "power-factor"  .= _aSynchronousMachinePF
    , "efficiency"  .= _aSynchronousMachineEfficiency
    , "locked-rotor-ratio"  .= _aSynchronousMachineLRR
    , "no-of-poles"  .= _aSynchronousMachinePoles
    , "symbol-object"        .= _aSynchronousMachineDiagramObject
    , "rating-object"        .= _aSynchronousMachineDiagramTagObject
    ]

instance FromJSON ASynchronousMachine where
  parseJSON = withObject "ASynchronousMachine" $ \o -> ASynchronousMachine
    <$> o .: "id"
    <*> o .: "name"
    <*> o .: "tag"
    <*> o .: "description"
    <*> o .: "base-voltage"
    <*> o .: "voltage"
    <*> o .: "rating"
    <*> o .: "power-factor"
    <*> o .: "efficiency"
    <*> o .: "locked-rotor-ratio"
    <*> o .: "no-of-poles"
    <*> o .: "symbol-object"
    <*> o .: "rating-object"

makeLenses ''ASynchronousMachine




loadASynchronousMachine :: String -> IO (Either ParseException ASynchronousMachine)
loadASynchronousMachine fname = do 
  content <- BS.readFile fname
  es <- Data.Yaml.decodeFileEither fname
  case es of 
    Left e -> putStrLn $ show e
    Right f -> putStrLn $ show  f
  return es



writeASynchronousMachine :: ASynchronousMachine -> IO ()
writeASynchronousMachine = encodeFile "sample.yaml" 


filterASynchronousMachine :: Either ParseException ASynchronousMachine -> Bool
filterASynchronousMachine x 
  | isLeft x = False
  | isRight x = True


loadAllASynchronousMachines :: FilePath -> IO [ASynchronousMachine]
loadAllASynchronousMachines fpath = do
  setCurrentDirectory fpath
  contents <- listDirectory fpath
  putStrLn $ show contents
  yamlContent <- mapM loadASynchronousMachine contents
  let filtered = rights yamlContent
  return filtered

writeAllASynchronousMachines :: FilePath -> [ASynchronousMachine] -> IO ()
writeAllASynchronousMachines fpath es = do
  forM_ es writeASynchronousMachine





aSynchronousMachineZpu :: ASynchronousMachine -> ℝ -> ℝ -> Complex Double
aSynchronousMachineZpu es mva v= zpu
  where
    zb = v*v/mva
    zpu = 1.0 :+ 2.0
