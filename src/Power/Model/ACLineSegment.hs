{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
module Power.Model.ACLineSegment where


-- import Control.Lens
import Control.Applicative
import Data.Foldable
import Data.Monoid
import qualified Data.Text as T
import Optics
import Optics.TH
import qualified Data.ByteString.Char8 as BS
import System.Directory
import Data.Yaml
import Data.Either
import Data.UUID
import Power.Model.DiagramObject

type ℝ = Double

data ACLineSegment = ACLineSegment 
  { _acLineSegmentID :: UUID
  , _acLineSegmentName :: T.Text
  , _acLineSegmentTag :: T.Text
  , _acLineSegmentDescription :: Maybe T.Text
  , _acLineSegmentFrom :: UUID
  , _acLineSegmentFromSequence :: Maybe Int
  , _acLineSegmentTo :: UUID
  , _acLineSegmentToSequence :: Maybe Int
  , _acLineSegmentBaseVoltage :: ℝ
  , _acLineSegmentVoltage :: ℝ
  , _acLineSegmentLength :: ℝ
  , _acLineSegmentRuns :: ℝ
  , _acLineSegmentR :: ℝ
  , _acLineSegmentR0 :: ℝ
  , _acLineSegmentX :: ℝ
  , _acLineSegmentX0 :: ℝ
  , _acLineSegmentG :: ℝ
  , _acLineSegmentG0 :: ℝ
  , _acLineSegmentB :: ℝ
  , _acLineSegmentB0 :: ℝ
  , _acLineSegmentDiagramObject :: DiagramObject
  , _acLineSegmentDiagramTagObject :: DiagramObject
  } deriving (Show, Eq)


instance ToJSON ACLineSegment where
  toJSON ACLineSegment {..} = object 
    [ "id" .= _acLineSegmentID
    , "name" .= _acLineSegmentName
    , "tag"  .= _acLineSegmentTag
    , "description" .= _acLineSegmentDescription
    , "base-voltage"     .= _acLineSegmentBaseVoltage
    , "voltage"     .= _acLineSegmentVoltage
    , "from" .= _acLineSegmentFrom
    , "from-sequence" .= _acLineSegmentFromSequence
    , "to" .= _acLineSegmentTo
    , "to-sequence" .= _acLineSegmentToSequence
    , "length" .= _acLineSegmentLength
    , "no-of-runs" .= _acLineSegmentRuns
    , "r" .= _acLineSegmentR
    , "r0" .= _acLineSegmentR0
    , "x" .= _acLineSegmentX
    , "x0" .= _acLineSegmentX0
    , "g" .= _acLineSegmentG
    , "g0" .= _acLineSegmentG0
    , "b" .= _acLineSegmentB
    , "b0" .= _acLineSegmentB0
    , "symbol-object"        .= _acLineSegmentDiagramObject
    , "rating-object"        .= _acLineSegmentDiagramTagObject
    ]

instance FromJSON ACLineSegment where
  parseJSON = withObject "ACLineSegment" $ \o -> ACLineSegment
    <$> o .: "id"
    <*> o .: "name"
    <*> o .: "tag"
    <*> o .: "description"
    <*> o .: "from"
    <*> o .:? "from-sequence"
    <*> o .: "to"
    <*> o .:? "to-sequence"
    <*> o .: "base-voltage"
    <*> o .: "voltage"
    <*> o .: "length"
    <*> o .: "no-of-runs"
    <*> o .: "r"
    <*> o .: "r0"
    <*> o .: "x"
    <*> o .: "x0"
    <*> o .: "g"
    <*> o .: "g0"
    <*> o .: "b"
    <*> o .: "b0"
    <*> o .: "symbol-object"
    <*> o .: "rating-object"

makeLenses ''ACLineSegment




loadACLineSegment :: String -> IO (Either ParseException ACLineSegment)
loadACLineSegment fname = do 
  content <- BS.readFile fname
  let es = Data.Yaml.decodeFileEither fname
  es



writeACLineSegment :: ACLineSegment -> IO ()
writeACLineSegment = encodeFile "sample.yaml" 


filterACLineSegment :: Either ParseException ACLineSegment -> Bool
filterACLineSegment x 
  | isLeft x = False
  | isRight x = True


loadAllACLineSegments :: FilePath -> IO [ACLineSegment]
loadAllACLineSegments fpath = do
  setCurrentDirectory fpath
  contents <- listDirectory fpath
  yamlContent <- mapM loadACLineSegment contents
  let filtered = rights yamlContent
  return filtered

writeAllACLineSegments :: FilePath -> [ACLineSegment] -> IO ()
writeAllACLineSegments fpath es = do
  forM_ es writeACLineSegment
