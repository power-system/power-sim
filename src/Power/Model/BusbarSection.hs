{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
module Power.Model.BusbarSection where


-- import Control.Lens
import Control.Applicative
import Data.Foldable
import Data.Monoid
import qualified Data.Text as T
import Optics
import Optics.TH
import qualified Data.ByteString.Char8 as BS
import System.Directory
import Data.Yaml
import Data.Either

import Data.UUID
import Power.Model.DiagramObject

type ℝ = Double

data BusbarSection = BusbarSection 
  { _busbarSectionID :: UUID
  , _busbarSectionName :: T.Text
  , _busbarSectionTag :: T.Text
  , _busbarSectionDescription :: Maybe T.Text
  , _busbarSectionVoltage :: ℝ
  , _busbarSectionCurrentRating :: ℝ
  , _busbarSectionBayCount :: Int
  , _busbarSectionDiagramObject :: DiagramObject
  , _busbarSectionDiagramTagObject :: DiagramObject
  } deriving (Show, Eq)


instance ToJSON BusbarSection where
  toJSON BusbarSection {..} = object 
    [ "id" .= _busbarSectionID
    , "name" .= _busbarSectionName
    , "tag"  .= _busbarSectionTag
    , "description" .= _busbarSectionDescription
    , "voltage"     .= _busbarSectionVoltage
    , "current-rating"  .= _busbarSectionCurrentRating
    , "no-of-bays"  .= _busbarSectionBayCount
    , "symbol-object"        .= _busbarSectionDiagramObject
    , "rating-object"        .= _busbarSectionDiagramTagObject
    ]

instance FromJSON BusbarSection where
  parseJSON = withObject "BusbarSection" $ \o -> BusbarSection
    <$> o .: "id"
    <*> o .: "name"
    <*> o .: "tag"
    <*> o .: "description"
    <*> o .: "voltage"
    <*> o .: "current-rating"
    <*> o .: "no-of-bays"
    <*> o .: "symbol-object"
    <*> o .: "rating-object"

makeLenses ''BusbarSection




loadBusbarSection :: String -> IO (Either ParseException BusbarSection)
loadBusbarSection fname = do 
  content <- BS.readFile fname
  let es = Data.Yaml.decodeFileEither fname
  es



writeBusbarSection :: BusbarSection -> IO ()
writeBusbarSection = encodeFile "sample.yaml" 


filterBusbarSection :: Either ParseException BusbarSection -> Bool
filterBusbarSection x 
  | isLeft x = False
  | isRight x = True


loadAllBusbarSections :: FilePath -> IO [BusbarSection]
loadAllBusbarSections fpath = do
  setCurrentDirectory fpath
  contents <- listDirectory fpath
  yamlContent <- mapM loadBusbarSection contents
  let filtered = rights yamlContent
  return filtered

writeAllBusbarSections :: FilePath -> [BusbarSection] -> IO ()
writeAllBusbarSections fpath es = do
  forM_ es writeBusbarSection
