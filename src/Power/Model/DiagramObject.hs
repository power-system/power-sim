{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
module Power.Model.DiagramObject where

import Data.Yaml
import qualified Data.Text as T
import Optics
import Optics.TH

data DiagramObject = DiagramObject 
  { _diagramObjectID :: Int
  , _diagramObjectName :: T.Text
  , _diagramObjectSequenceNumber :: Int
  , _diagramObjectAngle :: Int
  , _diagramObjectXPos :: Int
  , _diagramObjectYPos :: Int
  }deriving (Show, Eq)


makeLenses ''DiagramObject

instance FromJSON DiagramObject where
  parseJSON = withObject "DiagramObject" $ \o -> DiagramObject
    <$> o .: "id"
    <*> o .: "name"
    <*> o .: "sequence-number"
    <*> o .: "angle"
    <*> o .: "x-pos"
    <*> o .: "y-pos"


instance ToJSON DiagramObject where
  toJSON DiagramObject {..} = object 
    [ "id" .= _diagramObjectID
    , "name" .= _diagramObjectName
    , "sequence-number" .= _diagramObjectSequenceNumber
    , "angle" .= _diagramObjectAngle
    , "x-pos" .= _diagramObjectXPos
    , "y-pos" .= _diagramObjectYPos
    ]

