{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
module Power.Model.SynchronousMachine where


-- import Control.Lens
import Control.Applicative
import Data.Foldable
import Data.Monoid
import qualified Data.Text as T
import Optics
import Optics.TH
import qualified Data.ByteString.Char8 as BS
import System.Directory
import Data.Yaml
import Data.Either
import Data.UUID
import Power.Model.DiagramObject

import Data.Complex

type ℝ = Double

data SynchronousMachine = SynchronousMachine 
  { _synchronousMachineID :: UUID
  , _synchronousMachineName :: T.Text
  , _synchronousMachineTag :: T.Text
  , _synchronousMachineDescription :: Maybe T.Text
  , _synchronousMachineBaseVoltage :: ℝ
  , _synchronousMachineVoltage :: ℝ
  , _synchronousMachineRating :: ℝ
  , _synchronousMachineXdtt :: ℝ
  , _synchronousMachineXdsat :: ℝ
  , _synchronousMachinePF :: ℝ
  , _synchronousMachineR :: ℝ
  , _synchronousMachineDiagramObject :: DiagramObject
  , _synchronousMachineDiagramTagObject :: DiagramObject
  } deriving (Show, Eq)


instance ToJSON SynchronousMachine where
  toJSON SynchronousMachine {..} = object 
    [ "id" .= _synchronousMachineID
    , "name" .= _synchronousMachineName
    , "tag"  .= _synchronousMachineTag
    , "description" .= _synchronousMachineDescription
    , "base-voltage"     .= _synchronousMachineBaseVoltage
    , "voltage"     .= _synchronousMachineVoltage
    , "rating"  .= _synchronousMachineRating
    , "xdtt"  .= _synchronousMachineXdtt
    , "xdsat"  .= _synchronousMachineXdtt
    , "power-factor"  .= _synchronousMachinePF
    , "rg"  .= _synchronousMachineR
    , "symbol-object"        .= _synchronousMachineDiagramObject
    , "rating-object"        .= _synchronousMachineDiagramTagObject
    ]

instance FromJSON SynchronousMachine where
  parseJSON = withObject "SynchronousMachine" $ \o -> SynchronousMachine
    <$> o .: "id"
    <*> o .: "name"
    <*> o .: "tag"
    <*> o .: "description"
    <*> o .: "base-voltage"
    <*> o .: "voltage"
    <*> o .: "rating"
    <*> o .: "xdtt"
    <*> o .: "xdsat"
    <*> o .: "power-factor"
    <*> o .: "rg"
    <*> o .: "symbol-object"
    <*> o .: "rating-object"

makeLenses ''SynchronousMachine




loadSynchronousMachine :: String -> IO (Either ParseException SynchronousMachine)
loadSynchronousMachine fname = do 
  content <- BS.readFile fname
  let es = Data.Yaml.decodeFileEither fname
  es



writeSynchronousMachine :: SynchronousMachine -> IO ()
writeSynchronousMachine = encodeFile "sample.yaml" 


filterSynchronousMachine :: Either ParseException SynchronousMachine -> Bool
filterSynchronousMachine x 
  | isLeft x = False
  | isRight x = True


loadAllSynchronousMachines :: FilePath -> IO [SynchronousMachine]
loadAllSynchronousMachines fpath = do
  setCurrentDirectory fpath
  contents <- listDirectory fpath
  putStrLn $ show contents
  yamlContent <- mapM loadSynchronousMachine contents
  let filtered = rights yamlContent
  return filtered

writeAllSynchronousMachines :: FilePath -> [SynchronousMachine] -> IO ()
writeAllSynchronousMachines fpath es = do
  forM_ es writeSynchronousMachine


synchronousMachineZpu :: SynchronousMachine -> ℝ -> ℝ -> Complex Double
synchronousMachineZpu es mva v= zpu
  where
    zb = v*v/mva
    xddtt = _synchronousMachineXdtt es
    rg = _synchronousMachineR es
    zpu = rg :+ xddtt
