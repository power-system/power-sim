{-# LANGUAGE TemplateHaskell #-}
module Power.State where

import Power.Model.EnergySource
import Power.Model.EnergyConsumer
import Power.Model.SynchronousMachine
import Power.Model.ASynchronousMachine
import Power.Model.BusbarSection
import Power.Model.PowerTransformer
import Power.Model.ACLineSegment

import  Database.PostgreSQL.Simple (Connection, 
    ConnectInfo (..), Only (..), defaultConnectInfo, connect, query_, 
    close)
import  Data.Pool (Pool, createPool, withResource, destroyAllResources)
import Data.List
import Optics


connectionInfo :: ConnectInfo
connectionInfo =
  defaultConnectInfo {
      connectHost = "localhost"
    , connectPort = 5432
    , connectUser = "postgres2"
    , connectPassword = "12345678"
    , connectDatabase = "test_postgres2"
  }


myPool :: IO (Pool Connection)
myPool = createPool (connect connectionInfo) close 1 10 10

data State = State 
  { _dBPool :: Pool Connection
  , _energySources :: [EnergySource]
  , _energyConsumers :: [EnergyConsumer]
  , _synchronousMachines :: [SynchronousMachine]
  , _aSynchronousMachines :: [ASynchronousMachine]
  , _busbarSections :: [BusbarSection]
  , _powerTransformers :: [PowerTransformer]
  , _lines :: [ACLineSegment]
  } deriving (Show)


makeLenses ''State

insertEnergySource :: EnergySource -> State -> State
insertEnergySource es = over energySources (++ [es])

insertEnergyConsumer :: EnergyConsumer -> State -> State
insertEnergyConsumer load = over energyConsumers (++ [load])

