module Power.Analysis.Types where



class Solvable s where
  solve :: s -> IO ()


data BusType = PQBus | PVBus | SlackBus deriving (Show)

