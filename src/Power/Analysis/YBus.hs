module Power.Analysis.YBus where

import Data.Graph.Inductive.Graph
import Power.Model.Network
import Data.List (sortBy, group)
import Data.Map (fromListWith, toList)
import Data.Complex
import qualified Data.Vector as V





sortIndices :: Num a => ((Int, Int),a) -> ((Int, Int), a) -> Ordering
sortIndices ((x, y),_) ((a, b),_) = 
  if x < a then 
          LT 
  else if x > a then 
          GT
  else
          if y < b then 
                  LT
          else if y > b then 
                  GT
          else EQ

data SMatrix a = SMatrix (V.Vector Int) (V.Vector Int) (V.Vector a)

sparseRowVector :: SMatrix a -> V.Vector a
sparseRowVector = undefined

type YBus = SMatrix (Complex Double)


makeSparse :: (Num a) =>[((Int, Int), a)] -> SMatrix a
makeSparse val = let 
  clist = fromListWith (+) val
  indices = map fst val
  minRow = min $ map fst indices
  maxRow = max $ map fst indices
  minCol = min $ map snd indices
  maxCol = max $ map snd indices
  sortedList = sortBy sortIndices $ toList clist
  values = V.fromList $ map snd sortedList
  colList = V.fromList $ map snd $ map fst sortedList
  rowList = V.fromList $ map length $ group $ map fst $ map fst sortedList
  in 
  SMatrix rowList colList values


createYBus :: Network -> YBus
createYBus net = let 
  networkNodes = labNodes $ graphs net
  networkEdges = labEdges $ graphs net
  in 
  makeSparse $ Prelude.map (\(n1, n2, b)-> ((n1, n2), b)) networkEdges




getRow :: SMatrix a -> Int -> V.Vector a
getRow (SMatrix rows cols vals) r = undefined
