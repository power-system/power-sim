module Power.Analysis.LoadFlow where

import Numeric.LinearAlgebra.Data
import Data.Text as T
import Power.Analysis.Types
import Data.Vector.Storable as VS
import Data.Vector as V
import Data.Complex
import Data.Maybe

data LoadFlow = LoadFlow 
  { name :: T.Text
  , yBus :: Matrix C
  , busIndices :: V.Vector BusType
  , busVoltages :: V.Vector (Maybe C)
  , busPowers :: V.Vector (Maybe C)
  , busVoltageLimits :: V.Vector (Maybe (Double, Double))
  , busQLimits :: V.Vector (Maybe (Double, Double))
  }

type BusVoltages = V.Vector C

initBusVoltage :: (BusType , Maybe C) -> C
initBusVoltage (bt, v) = case bt of 
   PQBus -> 1.0 :+ 0.0
   PVBus -> case v of 
                Just x -> x
                Nothing -> 1.0 :+ 0.0
   SlackBus -> 1.0 :+ 0.0

initializeBusVoltages :: LoadFlow -> V.Vector C
initializeBusVoltages lf = let
    bi = busIndices lf
    in V.map initBusVoltage $ V.zip (busIndices lf) (busVoltages lf)


iterateOnce :: LoadFlow -> BusVoltages -> BusVoltages
iterateOnce lf v = let
    rows = toRows $ yBus lf
    iterLF = iterateOnce lf
    in  
    V.fromList $ Prelude.map (\x -> 0.0 :+ 0.0) rows

solveLoadFlow :: LoadFlow -> IO ()
solveLoadFlow lf@(LoadFlow _ yb _ _ _ _ _ ) = do
  let rows = toRows yb
  let l = Prelude.map (\x -> VS.length x) rows
  return ()

instance Solvable LoadFlow where
  solve = solveLoadFlow
