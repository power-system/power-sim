{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE OverloadedStrings #-}
module Main where

import Prelude
import Lib
import Diagrams.Backend.SVG
import Diagrams.Prelude
import Diagrams.Backend.SVG.CmdLine

import Control.Concurrent.Chan
import Data.Maybe
import Data.Graph.Inductive.NodeMap
import Data.UUID

import Power.State
import Power.Model.EnergySource
import Power.Model.EnergyConsumer
import Power.Model.SynchronousMachine
import Power.Model.ASynchronousMachine
import Power.Model.BusbarSection
import Power.Model.PowerTransformer
import Power.Model.ACLineSegment
import Power.Model.Network
import Data.Complex
import Data.Graph.Inductive.Graph
import Prettyprinter

import Data.Graph.Inductive.Dot

import Power.Model.Graphics

myCircle :: Diagram B
myCircle = circle 1

main :: IO ()
main = do 
    let size = mkSizeSpec $ V2 (Just 600) (Just 600)
    pool <- myPool
    es<-loadAllEnergySources "/usr/home/shashank/model/energy-sources"
    loads<-loadAllEnergyConsumers "/usr/home/shashank/model/energy-consumers"
    synchronousMachines <- loadAllSynchronousMachines "/usr/home/shashank/model/synchronous-machines"
    motors <- loadAllASynchronousMachines "/usr/home/shashank/model/asynchronous-machines"
    busbars <- loadAllBusbarSections "/usr/home/shashank/model/busbar-sections"
    transformers <- loadAllPowerTransformers "/usr/home/shashank/model/power-transformers"
    lines <- loadAllACLineSegments "/usr/home/shashank/model/line-segments"
    let st = State pool es loads synchronousMachines motors busbars transformers lines
    let net = createNetwork st YBUS 
    let newNet = createNetworkEdges st net
    let dot = showDot (fglToDot $ graphs newNet)
    writeFile "/usr/home/shashank/file.dot" dot
    putStrLn $ show st
    prettyPrint $ graphs newNet
    let windLinks = concat $ map (\x -> createTransformerWindingLinks x newNet) $ _powerTransformers st

    let qDias = drawEnergySources $ _energySources st
    renderSVG "/home/shashank/sample.svg" size qDias
    putStrLn $ show windLinks
