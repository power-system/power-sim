# Power-Sim
Power Sim (P-Sim) is an electrical network modelling and simulation software. 

## Introduction
The basic steps for performing a power system study is to 
1. Create a network
2. Design a study case (study parameters) required for the application
3. Perform the system study on the network with the defined study case
4. Output the results in tabular form.

*P-Sim* is being designed to perform all the above 4 steps.

## Design Choices

### Network Modelling
Network and its elements are modelled using YAML files. With this method, one can edit the 
element parameters using a text editor.

### Design Case
As with the network model, the design cases are also modelled in YAML format.

### Study Cases
The study case on the network is run using command line interface. Both the network model and the study case
are specified as inputs to the CLI 

### Output
The results are generated in _tsv_ file format.


## Installation

P-Sim is built using haskell stack.

```
> stack build
> stack test
> stack install
```

### Usage
Once __P-Sim__ is installed, once could use the software as 

```
> power-sim <model-directory> <study-case-file> -o <results>
```

## Libraries Used

1. fgl
2. Vector
3. Diagrams
4. Cassava
5. HsYaml
6. hmatrix

## License
See [LICENSE](LICENSE) file.

